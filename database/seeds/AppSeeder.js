'use strict'

const Factory = use('Factory')

class AppSeeder {
  async run() {
    const company = await Factory.model('App/Models/Company').make()
    const roles = await Factory.model('Role').createMany(2, ['master', 'commercial'])

    const user = await Factory.model('App/Models/User').create()

    await user.company().associate(company)

    await user.roles().attach(roles[0].id)
    await user.roles().attach(roles[1].id)
  }
}

module.exports = AppSeeder

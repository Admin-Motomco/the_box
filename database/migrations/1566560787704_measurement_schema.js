'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MeasurementSchema extends Schema {
  up () {
    this.create('measurements', table => {
      table.increments()
      table.string('serialnumber').notNullable()
      table.json('data')
      table.json('sensors')
      table.json('troubles')
      table.string('grain')
      table.string('password')
      table.string('curveId')
      table.float('capx')
      table.float('dialT')
      table.float('dialN')
      table.float('dialC')
      table.float('humidity')
      table.float('density')
      table.timestamps()
    })
  }

  down () {
    this.drop('measurements')
  }
}

module.exports = MeasurementSchema

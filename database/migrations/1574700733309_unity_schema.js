'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnitySchema extends Schema {
  up () {
    this.table('unities', (table) => {
      // alter table
      table.string('lat')
      table.string('lng')
    })
  }

  down () {
    this.table('unities', (table) => {
      // reverse alternations
      table.dropColumn('lng')
      table.dropColumn('lat')
    })
  }
}

module.exports = UnitySchema

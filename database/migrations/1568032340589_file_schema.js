'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FileSchema extends Schema {
  up () {
    this.table('files', (table) => {
      // alter table
      table.boolean('downloaded').defaultTo(false).notNullable()
    })
  }

  down () {
    this.table('files', (table) => {
      // reverse alternations
      table.dropColumn('downloaded')
    })
  }
}

module.exports = FileSchema

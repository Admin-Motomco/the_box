'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnitySchema extends Schema {
  up () {
    this.table('unities', (table) => {
      // alter table
      table.dropColumn('cidade')
      table.dropColumn('estado')
      table.string('city')
      table.string('state')
    })
  }

  down () {
    this.table('unities', (table) => {
      // reverse alternations
      table.dropColumn('city')
      table.dropColumn('state')
      table.string('cidade')
      table.string('estado')
    })
  }
}

module.exports = UnitySchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EquipmentSchema extends Schema {
  up () {
    this.create('equipment', table => {
      table.increments()
      table.string('name').notNullable()
      table.string('model')
      table
        .string('serialnumber')
        .notNullable()
        .unique()
      table
        .integer('unity_id')
        .unsigned()
        .references('id')
        .inTable('unities')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table
        .integer('company_id')
        .unsigned()
        .references('id')
        .inTable('companies')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.boolean('status')
      table.boolean('active')
      table.string('position')
      table.string('ip')
      table.string('sha1Curve')
      table.string('grain')
      table.float('idealRangeMin')
      table.float('idealRangeMax')
      table.timestamps()
    })
  }

  down () {
    this.drop('equipment')
  }
}

module.exports = EquipmentSchema

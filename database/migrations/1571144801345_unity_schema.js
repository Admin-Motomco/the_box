'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnitySchema extends Schema {
  up () {
    this.table('unities', table => {
      // alter table
      table.dropColumn('city')
      table.dropColumn('state')
      table
        .integer('city_id')
        .unsigned()
        .references('id')
        .inTable('cities')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table
        .integer('state_id')
        .unsigned()
        .references('id')
        .inTable('states')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
    })
  }

  down () {
    this.table('unities', table => {
      // reverse alternations
      table.dropColumn('city_id')
      table.dropColumn('state_id')
      table.string('state')
      table.string('city')
    })
  }
}

module.exports = UnitySchema

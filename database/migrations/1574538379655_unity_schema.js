'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnitySchema extends Schema {
  up () {
    this.table('unities', (table) => {
      // alter table
      table.string('address')
      table.integer('number')
    })
  }

  down () {
    this.table('unities', (table) => {
      // reverse alternations
      table.dropColumn('number')
      table.dropColumn('address')
    })
  }
}

module.exports = UnitySchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnitySchema extends Schema {
  up () {
    this.create('unities', table => {
      table.increments()
      table
        .integer('company_id')
        .references('id')
        .inTable('companies')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.string('name')
      table.string('cnpj')
      table.string('phone')
      table.string('cidade')
      table.string('estado')
      table.string('contact_name')
      table.timestamps()
    })
  }

  down () {
    this.drop('unities')
  }
}

module.exports = UnitySchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EquipmentSchema extends Schema {
  up () {
    this.table('equipment', (table) => {
      // alter table
      table.boolean('hasFile').defaultTo(false)
    })
  }

  down () {
    this.table('equipment', (table) => {
      // reverse alternations
      table.dropColumn('hasFile')
    })
  }
}

module.exports = EquipmentSchema

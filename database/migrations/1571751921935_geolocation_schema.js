'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GeolocationSchema extends Schema {
  up () {
    this.create('geolocations', table => {
      table.increments()
      table
        .integer('equipment_id')
        .unsigned()
        .references('id')
        .inTable('equipment')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.string('serialnumber')
      table.float('lat')
      table.float('lng')
      table.timestamps()
    })
  }

  down () {
    this.drop('geolocations')
  }
}

module.exports = GeolocationSchema

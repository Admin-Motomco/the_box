'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MeasurementSchema extends Schema {
  up () {
    this.table('measurements', table => {
      // alter table
      table.text('sha1')
      table.text('sign')
      table.text('signature')
      table.text('pubkey')
    })
  }

  down () {
    this.table('measurements', table => {
      // reverse alternations
      table.dropColumn('sha1')
      table.dropColumn('sign')
      table.dropColumn('signature')
      table.dropColumn('pubkey')
    })
  }
}

module.exports = MeasurementSchema

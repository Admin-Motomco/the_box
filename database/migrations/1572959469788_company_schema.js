'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.table('companies', table => {
      // alter table
      table.dropColumn('state_id')
      table.dropColumn('city_id')
      table.string('state')
      table.string('city')
      table.string('address')
      table.string('number')
      table.string('obs').nullable()
    })
  }

  down () {
    this.table('companies', table => {
      // reverse alternations
      table.dropColumn('obs')
      table.dropColumn('number')
      table.dropColumn('address')
      table.dropColumn('city')
      table.dropColumn('state')
      table
        .integer('city_id')
        .unsigned()
        .references('id')
        .inTable('cities')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table
        .integer('state_id')
        .unsigned()
        .references('id')
        .inTable('states')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
    })
  }
}

module.exports = CompanySchema

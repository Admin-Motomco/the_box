'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MeasurementSchema extends Schema {
  up () {
    this.table('measurements', table => {
      // alter table
      table.dropColumn('data')
      table.dropColumn('sensors')
      table.dropColumn('troubles')
      table.dropColumn('density')
      table.boolean('blocked')
      table.string('sha1')
      table.string('sign')
      table.string('signature')
      table.string('pubkey')
      table.float('temperature')
      table.float('ph')
      table.string('idcalib')
    })
  }

  down () {
    this.table('measurements', table => {
      // reverse alternations
      table.dropColumn('blocked')
      table.dropColumn('sha1')
      table.dropColumn('sign')
      table.dropColumn('signature')
      table.dropColumn('pubkey')
      table.dropColumn('temperature')
      table.dropColumn('ph')
      table.dropColumn('idcalib')
      table.json('data')
      table.json('sensors')
      table.json('troubles')
      table.float('density')
    })
  }
}

module.exports = MeasurementSchema

'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.table('companies', table => {
      // alter table
      table.dropColumn('cidade')
      table.dropColumn('estado')
    })
  }

  down () {
    this.table('companies', table => {
      // reverse alternations
      table.string('cidade')
      table.string('estado')
    })
  }
}

module.exports = CompanySchema

'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StatesSchema extends Schema {
  up () {
    this.create('states', table => {
      table.increments()
      table.string('sigla')
      table.string('nome')
      table.json('regiao')
      table.timestamps()
    })
  }

  down () {
    this.drop('states')
  }
}

module.exports = StatesSchema

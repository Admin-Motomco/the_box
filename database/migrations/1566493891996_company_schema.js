'use strict'

const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.create('companies', table => {
      table.increments()
      table.string('name')
      table.string('cnpj')
      table.string('phone')
      table.string('cidade')
      table.string('estado')
      table.string('site')
      table.string('contact_name')
      table.timestamps()
    })
  }

  down () {
    this.drop('companies')
  }
}

module.exports = CompanySchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MeasurementSchema extends Schema {
  up () {
    this.table('measurements', table => {
      // alter table
      table.dropColumn('capx')
      table.dropColumn('dialT')
      table.dropColumn('dialN')
      table.dropColumn('dialC')
    })
  }

  down () {
    this.table('measurements', table => {
      // reverse alternations
      table.float('capx')
      table.float('dialT')
      table.float('dialN')
      table.float('dialC')
    })
  }
}

module.exports = MeasurementSchema

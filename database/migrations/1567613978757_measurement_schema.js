'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MeasurementSchema extends Schema {
  up () {
    this.table('measurements', table => {
      // alter table
      table.dropColumn('sha1')
      table.dropColumn('sign')
      table.dropColumn('signature')
      table.dropColumn('pubkey')
    })
  }

  down () {
    this.table('measurements', table => {
      // reverse alternations
      table.string('sha1')
      table.string('sign')
      table.string('signature')
      table.string('pubkey')
    })
  }
}

module.exports = MeasurementSchema

'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.table('companies', table => {
      // alter table
      table.integer('contract_number')
      table.integer('contract_time')
    })
  }

  down () {
    this.table('companies', table => {
      // reverse alternations
      table.dropColumn('contract_number')
      table.dropColumn('contract_time')
    })
  }
}

module.exports = CompanySchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FileSchema extends Schema {
  up () {
    this.table('files', table => {
      // alter table
      table
        .integer('equipment_id')
        .unsigned()
        .references('id')
        .inTable('equipment')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
    })
  }

  down () {
    this.table('files', table => {
      // reverse alternations
      table.dropColumn('equipment_id')
    })
  }
}

module.exports = FileSchema

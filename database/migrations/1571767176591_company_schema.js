'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.table('companies', table => {
      // alter table
      table.dropColumn('contact_name')
      table.string('first_email')
      table.string('second_email')
      table.string('third_email')
    })
  }

  down () {
    this.table('companies', table => {
      // reverse alternations
      table.dropColumn('third_email')
      table.dropColumn('second_email')
      table.dropColumn('first_email')
      table.string('contact_name')
    })
  }
}

module.exports = CompanySchema

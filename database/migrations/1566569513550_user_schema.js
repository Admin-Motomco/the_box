'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.table('users', table => {
      // alter table
      table
        .integer('company_id')
        .references('id')
        .inTable('companies')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table
        .integer('unity_id')
        .references('id')
        .inTable('unities')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
    })
  }

  down () {
    this.table('users', table => {
      table.dropColumn('company_id')
      table.dropColumn('unity_id')
    })
  }
}

module.exports = UserSchema

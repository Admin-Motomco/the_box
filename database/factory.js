'use strict'

const Env = use('Env')
const Factory = use('Factory')

// Company Factory
Factory.blueprint('App/Models/Company', () => {
  return {
    name: 'Loc Solution',
    cnpj: '18.700.987/0001-12',
    phone: '(41) 3241-4123',
    city: 'Curitiba',
    state: 'Paraná',
    site: 'https://motomco.com.br/site/'
  }
})

// User Factory
Factory.blueprint('App/Models/User', () => {
  return {
    username: 'master',
    password: Env.get('USER_MASTER_PASSWORD'),
    email: 'engenharia5@motomcogroup.com'
  }
})

// // Role Factory
// Factory.blueprint('Role', () => {
//   return {
//     slug: 'master',
//     name: 'Master',
//     description: 'Master of system'
//   }
// })

Factory.blueprint('Role', (fake, index, data) => {
  return {
    slug: data[index],
    name: data[index],
    description: `${data[index]} of system`
  }
})

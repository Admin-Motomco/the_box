'use strict';

class EquipmentSocketController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
    console.log(socket.id)
  }
}

module.exports = EquipmentSocketController

'use strict'

const Role = use('Role')

class RoleController {
  async index () {
    const roles = await Role.query()
      .with('permissions')
      .fetch()

    return roles
  }

  async show ({ params, response }) {
    try {
      const role = await Role.findOrFail(params.id)

      await role.load('permissions')

      return role
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível resgatar esse papel, ele existe?'
        }
      })
    }
  }

  async store ({ request, response }) {
    try {
      const { permissions, ...data } = request.only([
        'name',
        'slug',
        'description',
        'permissions'
      ])

      const role = await Role.create(data)

      if (permissions) {
        await role.permissions().attach(permissions)
      }

      await role.load('permissions')

      return role
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível criar o papel.'
        }
      })
    }
  }

  async update ({ request, response, params }) {
    try {
      const { permissions, ...data } = request.only([
        'name',
        'slug',
        'description',
        'permissions'
      ])

      const role = await Role.findOrFail(params.id)

      role.merge(data)

      await role.save()

      if (permissions) {
        await role.permissions().sync(permissions)
      }

      await role.load('permissions')

      return role
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível atualizar o papel.'
        }
      })
    }
  }

  async destroy ({ params }) {
    const role = await Role.findOrFail(params.id)

    await role.delete()
  }
}

module.exports = RoleController

'use strict'

const Env = use('Env')

const GeoLocation = use('App/Models/Geolocation')
const Equipment = use('App/Models/Equipment')

class GeolocationController {
  async index({ response, params }) {
    try {
      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        params.serialnumber
      )
      const geolocations = await GeoLocation.query()
        .where('serialnumber', equipment.serialnumber)
        .orderBy('created_at', 'desc')
        .fetch()

      return geolocations
    } catch (err) {
      return response.status(404).send({
        error: {
          message: 'Não foi possível resgatar a geolocalização'
        }
      })
    }
  }

  async store({ request, response }) {
    try {
      const pass = Env.get('GEOLOCATION_PASSWORD')

      const password = request.input('password')
      const data = request.only(['serialnumber', 'lat', 'lng'])

      if (pass !== password) {
        return response.status(403).send({
          error: {
            message: 'Acesso não autorizado'
          }
        })
      }

      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        data.serialnumber
      )

      data.equipment_id = equipment.id

      const geolocation = await GeoLocation.create(data)

      return geolocation
    } catch (err) {
      console.log(err)
      response.status(404).send({
        error: {
          message: 'Não foi possível criar a geolocalizaçào'
        }
      })
    }
  }

  async show({ params, response }) {
    try {
      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        params.serialnumber
      )
      const geolocation = await GeoLocation.query()
        .where('serialnumber', params.serialnumber)
        .limit(1)
        .orderBy('id', 'desc')
        .fetch()

      return geolocation
    } catch (error) {
      return response.status(404).send({
        error: {
          message: 'Não encontrado'
        }
      })
    }
  }

  async update({ params, request, response }) { }

  async destroy({ params, request, response }) { }
}

module.exports = GeolocationController

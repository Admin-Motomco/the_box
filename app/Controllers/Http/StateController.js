'use strict'
const Axios = use('axios')

const State = use('App/Models/State')

class StateController {
  async store ({ request, response }) {
    try {
      Axios.get(
        'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
      ).then(async res => {
        for (const state of res.data) {
          const newState = await State.create(state)
        }
      })
    } catch (err) {
      return response.status(404).send({
        error: {
          message: 'Não foi possível criar os estados'
        }
      })
    }
  }

  async index ({ response }) {
    try {
      const states = State.all()

      return states
    } catch (err) {
      response.status(404).send({
        error: {
          message: 'Não foi possível listar os estados'
        }
      })
    }
  }
}
module.exports = StateController

'use strict'

const Measurement = use('App/Models/Measurement')
const Equipment = use('App/Models/Equipment')
const User = use('App/Models/User')
const moment = use('moment')

class MeasurementController {
  async index({ params, auth, response, request }) {
    try {
      const query = request.get()

      if (!params.serialnumber) {
        return response.status(404).send({
          error: {
            message: 'O número serial do equipamento foi informado?'
          }
        })
      }

      const { id } = auth.user
      const user = await User.findOrFail(id)

      if ((await user.is('master')) || (await user.is('commercial'))) {
        if (!query.startDate || !query.finalDate) {
          const measurements = await Measurement.query()
            .where('serialnumber', params.serialnumber)
            .limit(1000)
            .orderBy('id', 'desc')
            .fetch()
          return measurements
        } else {
          const measurements = await Measurement.query()
            .where('serialnumber', params.serialnumber)
            .whereBetween('created_at', [
              moment(query.startDate).startOf('day'),
              moment(query.finalDate).endOf('day')
            ])
            .orderBy('id', 'desc')
            .fetch()
          return measurements
        }
      }

      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        params.serialnumber
      )

      if (equipment.company_id !== user.company_id) {
        const error = await response.status(403).send({
          error: {
            message: 'Você não possui as devidas permissões para essa ação'
          }
        })
        return error
      }

      if (!query.startDate || !query.finalDate) {
        const measurements = await Measurement.query()
          .where('serialnumber', params.serialnumber)
          .fetch()
        return measurements
      } else {
        const measurements = await Measurement.query()
          .where('serialnumber', params.serialnumber)
          .whereBetween('created_at', [
            moment(query.startDate).startOf('day'),
            moment(query.finalDate).endOf('day')
          ])
          .fetch()
        return measurements
      }
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível resgatar a medição'
        }
      })
    }
  }

  async store({ request, response }) {
    try {
      const data = request.only([
        'serialnumber',
        'grain',
        'password',
        'curveId',
        'capx',
        'dialT',
        'dialN',
        'dialC',
        'humidity',
        'density',
        'blocked',
        'sha1',
        'sign',
        'signature',
        'pubkey',
        'temperature',
        'ph',
        'idcalib'
      ])

      const measurement = await Measurement.create(data)

      return response.status(200).send({"status": "ok", "appId": request.body.appId})
    } catch (err) {
      console.log(err)
      return response.status(404).send({
        error: {
          message: 'Não foi possível criar a medição'
        }
      })
    }
  }

  async show({ params, response, auth }) {
    try {
      const { id } = auth.user
      const user = await User.findOrFail(id)

      const measurement = await Measurement.query()
        .where('serialnumber', params.serialnumber)
        .limit(1)
        .orderBy('id', 'desc')
        .fetch()

      if ((await user.is('master')) || (await user.is('commercial'))) {
        return measurement
      }

      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        params.serialnumber
      )

      if (equipment.company_id !== user.company_id) {
        return response.status(403).send({
          error: {
            message: 'Você não possui as devidas permissões para essa ação'
          }
        })
      }
      return measurement
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível resgatar a medição'
        }
      })
    }
  }

  async update({ params, request, response }) {
    try {
      const measurement = await Measurement.findOrFail(params.id)

      const data = request.only([
        'serialnumber',
        'data',
        'sensors',
        'troubles',
        'grain',
        'password',
        'curveId',
        'capx',
        'dialT',
        'dialN',
        'dialC',
        'humidity',
        'density'
      ])

      measurement.merge(data)

      await measurement.save()

      return measurement
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível atualizar a medição'
        }
      })
    }
  }

  async destroy({ params, response }) {
    try {
      const measurement = await Measurement.findOrFail(params.id)

      measurement.delete()
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível deletar a medição'
        }
      })
    }
  }
}

module.exports = MeasurementController

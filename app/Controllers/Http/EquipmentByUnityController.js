'use strict';
const Equipment = use('App/Models/Equipment')
const User = use('App/Models/User')

class EquipmentByUnityController {
  async index ({ params, response, auth }) {
    try {
      const { id } = auth.user

      const requestUser = await User.findOrFail(id)

      if (
        (await requestUser.is('master')) ||
        (await requestUser.is('commercial'))
      ) {
        const { unity_id } = params

        const equipments = await Equipment.query()
          .where('unity_id', unity_id)
          .with('company')
          .with('unity')
          .with('file')
          .fetch()

        return equipments
      } else {
        const equipments = await Equipment.query()
          .where('unity_id', requestUser.unity_id)
          .with('company')
          .with('unity')
          .with('file')
          .fetch()

        return equipments
      }
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível resgatar os equipamentos.'
        }
      })
    }
  }
}

module.exports = EquipmentByUnityController

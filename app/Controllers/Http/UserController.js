// eslint-disable-next-line semi
'use strict';
const User = use('App/Models/User')
const Role = use('Role')
const Yup = use('yup')

class UserController {
    async index() {
        const users = await User.query()
            .with('company')
            .with('roles')
            .with('permissions')
            .fetch()

        return users
    }

    async show({ request, response, params }) {
        try {
            const user = await User.find(params.id)

            return user
        } catch (error) {
            return response.status(404).send({
                error: {
                    message: 'Usuário não encontrado.'
                }
            })
        }
    }

    async store({ request, auth, response }) {
        try {
            const schema = Yup.object().shape({
                username: Yup.string().required(),
                password: Yup.string().required(),
                email: Yup.string().required(),
                company_id: Yup.number(),

                name: Yup.string().required()
            })

            const { permissions, roles, ...data } = request.only([
                'username',
                'password',
                'email',
                'company_id',
                'unity_id',
                'permissions',
                'name',
                'roles'
            ])

            schema.validate(data).catch(function(err) {
                console.log(err)
            })

            const masterRole = await Role.findByOrFail('slug', 'master')
            const commercialRole = await Role.findByOrFail('slug', 'commercial')

            if (!(await schema.isValid(data))) {
                return response.status(400).send({
                    error: {
                        message: 'Erro de validação'
                    }
                })
            }

            const { id } = auth.user

            const requestUser = await User.findOrFail(id)

            if (roles) {
                if (
                    (await requestUser.is('company_master')) &&
                    (await roles.includes(commercialRole.id))
                ) {
                    return response.status(403).send({
                        error: {
                            message: 'Você não possui permissão para criar um usuário com esse papel'
                        }
                    })
                }
                if (
                    (await requestUser.is('company_master')) &&
                    (await roles.includes(masterRole.id))
                ) {
                    return response.status(403).send({
                        error: {
                            message: 'Você não possui permissão para criar um usuário com esse papel'
                        }
                    })
                }

                if (
                    roles.includes(masterRole.id) &&
                    !(await requestUser.is('master'))
                ) {
                    return response.status(403).send({
                        error: {
                            message: 'Você não possui permissão para criar um usuário com esse papel'
                        }
                    })
                }
            }

            const usernameExists = await User.findBy('username', data.username)
            const emailExists = await User.findBy('email', data.email)

            if (usernameExists) {
                return response.status(404).send({
                    error: {
                        message: 'Username em uso.'
                    }
                })
            }

            if (emailExists) {
                return response.status(404).send({
                    error: {
                        message: 'E-mail já cadastrado.'
                    }
                })
            }

            const user = await User.create(data)

            if (requestUser.is('company_master')) {
                user.company_id = requestUser.company_id
            }

            if (roles) {
                await user.roles().attach(roles)
            }

            if (permissions) {
                await user.permissions().attach(permissions)
            }

            await user.loadMany(['roles', 'permissions'])

            return user
        } catch (err) {
            console.log(err)
            return response.status(404).send({
                error: {
                    message: 'Não foi possível criar o usuário.'
                }
            })
        }
    }

    async update({ request, params }) {
        const { permissions, roles, ...data } = request.only([
            'username',
            'password',
            'email',
            'company_id',
            'unity_id',
            'permissions',
            'name',
            'roles'
        ])

        const user = await User.findOrFail(params.id)

        user.merge(data)

        await user.save()

        if (roles) {
            await user.roles().sync(roles)
        }

        if (permissions) {
            await user.permissions().sync(permissions)
        }

        await user.loadMany(['roles', 'permissions'])

        return user
    }

    async destroy({ params, response }) {
        try {
            const user = await User.findOrFail(params.id)

            user.delete()
        } catch (err) {
            return response.status(err.status).send({
                error: {
                    message: 'Não foi possível deletar o usuário'
                }
            })
        }
    }
}

module.exports = UserController
'use strict';

const Yup = use('yup')
const Company = use('App/Models/Company')

class CompanyController {
  async index ({ request, response }) {
    try {
      let { page } = request.get()

      page = page || 1

      const companies = await Company.query()
        .paginate(page || 1, 10)

      return companies
    } catch (err) {
      console.log(err)
      response.status(404).send({
        error: {
          message: 'Não foi possível resgatar as empresas'
        }
      })
    }
  }

  async store ({ request, response }) {
    try {
      const schema = Yup.object().shape({
        name: Yup.string().required(),
        cnpj: Yup.string().required(),
        phone: Yup.string()
          .required()
          .min(11),
        first_email: Yup.string().required(),
        second_email: Yup.string().nullable(),
        third_email: Yup.string().nullable(),
        city_id: Yup.number(),
        state_id: Yup.number(),
        site: Yup.string()
      })

      const data = request.only([
        'name',
        'cnpj',
        'phone',
        'first_email',
        'second_email',
        'third_email',
        'city',
        'state',
        'site',
        'contract_number',
        'contract_time',
        'address',
        'number'
      ])

      // schema.validate(data).catch(function (err) {
      //   console.log(err.errors)
      // })

      if (!(await schema.isValid(data))) {
        return response.status(400).send({
          error: {
            message: 'Erro de validação'
          }
        })
      }

      const company = await Company.create(data)

      return company
    } catch (err) {
      console.log(err)
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível criar a empresa'
        }
      })
    }
  }

  async show ({ params, response }) {
    try {
      const company = await Company.findOrFail(params.id)

      return company
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível resgatar a empresa'
        }
      })
    }
  }

  async update ({ params, request, response }) {
    try {
      const schema = Yup.object().shape({
        name: Yup.string().required(),
        cnpj: Yup.string().required(),
        phone: Yup.string()
          .required()
          .min(11),
        first_email: Yup.string().required(),
        second_email: Yup.string(),
        third_email: Yup.string(),
        city_id: Yup.number().required(),
        state_id: Yup.number().required(),
        site: Yup.string()
      })

      const data = request.only([
        'name',
        'cnpj',
        'phone',
        'first_email',
        'second_email',
        'third_email',
        'city',
        'state',
        'site',
        'contract_number',
        'contract_time',
        'address',
        'number'
      ])

      schema.validate(data).catch(function (err) {
        console.log(err.errors)
      })

      if (!(await schema.isValid(data))) {
        return response.status(400).send({
          error: {
            message: 'Erro de validação'
          }
        })
      }

      const company = await Company.findOrFail(params.id)

      company.merge(data)

      await company.save()

      return company
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível editar a empresa'
        }
      })
    }
  }

  async destroy ({ params }) {
    const company = await Company.findOrFail(params.id)

    company.delete()
  }
}

module.exports = CompanyController

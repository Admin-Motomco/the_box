'use strict'
const OpenCage = use('opencage-api-client')
const Yup = use('yup')
const Unity = use('App/Models/Unity')
const Company = use('App/Models/Company')
const User = use('App/Models/User')
const City = use('App/Models/City')
const State = use('App/Models/State')

class UnityController {
  async index({ params, response }) {
    try {
      const unities = await Unity.query()
        .where('company_id', params.company_id)
        .with('company')
        .fetch()

      return unities
    } catch (err) {
      response.status(404).send({
        error: {
          message: 'Não foi possível resgatar as unidades.'
        }
      })
    }
  }

  async store({ request, response, auth }) {
    try {
      const { id } = auth.user

      const user = await User.findOrFail(id)

      const schema = Yup.object().shape({
        name: Yup.string().required(),
        cnpj: Yup.string().required(),
        phone: Yup.string()
          .required()
          .min(11),
        contact_name: Yup.string().required(),
        city_id: Yup.number().required(),
        state_id: Yup.number().required(),
        company_id: Yup.number().required(),
        address: Yup.string(),
        number: Yup.number()
      })

      const data = await request.only([
        'company_id',
        'name',
        'cnpj',
        'phone',
        'city_id',
        'state_id',
        'contact_name',
        'address',
        'number'
      ])

      const { nome: cityName } = await City.find(data.city_id)
      const { nome: stateName } = await State.find(data.state_id)

      const companyExists = await Company.find(data.company_id)

      if (!companyExists) {
        return response.status(404).send({
          error: {
            message: 'Empresa inexistente.'
          }
        })
      }

      schema.validate(data).catch(function (err) {
        console.log(err.errors)
      })

      if (!(await schema.isValid(data))) {
        return response.status(400).send({
          error: {
            message: 'Erro de validação'
          }
        })
      }
      const geolocation = await OpenCage.geocode({ q: `${data.address}, ${data.number}, ${cityName}, ${stateName}` })
      // const geolocation = await OpenCage.geocode({ q: 'Rua Professor Pedro Viriato Parigot de Souza, 1900, Diamante do Norte, Paraná' })

      const parsedGeo = geolocation.results[0]

      const unity = await Unity.create({ ...data, lat: parsedGeo.annotations.DMS.lat, lng: parsedGeo.annotations.DMS.lng })

      return unity
    } catch (err) {
      console.log(err)
      return response.status(404).send({
        error: {
          message: 'Não foi possível criar a unidade.'
        }
      })
    }
  }

  async show({ params, response }) {
    try {
      const unity = await Unity.findOrFail(params.id)

      return unity
    } catch (err) {
      console.log(err)
      return response.status(err.status).send({

        error: {
          message: 'Não foi possível resgatar a unidade.'
        }
      })
    }
  }

  async update({ params, request, response }) {
    try {
      const data = await request.only([
        'company_id',
        'name',
        'cnpj',
        'phone',
        'city_id',
        'state_id',
        'contact_name',
        'address',
        'number'
      ])

      const unity = await Unity.findOrFail(params.id)

      unity.merge(data)

      await unity.save()

      return unity
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível atualizar a unidade.'
        }
      })
    }
  }

  async destroy({ params, response }) {
    try {
      const unity = await Unity.findOrFail(params.id)

      unity.delete()
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível deletar a unidade.'
        }
      })
    }
  }
}

module.exports = UnityController

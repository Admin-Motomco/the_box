'use strict'

const Equipment = use('App/Models/Equipment')

class EquipmentController {
  async index() {
    const equipments = await Equipment.query()
      .with('company')
      .with('unity')
      .with('file')
      .fetch()

    return equipments
  }

  async store({ request, response }) {
    try {
      const data = request.only([
        'name',
        'model',
        'serialnumber',
        'unity_id',
        'company_id',
        'status',
        'active',
        'position',
        'ip',
        'sha1Curve',
        'grain',
        'idealRangeMin',
        'idealRangeMax'
      ])

      if (!data.status) {
        data.status = false
      }

      const equipment = await Equipment.create(data)

      return equipment
    } catch (err) {
      console.log(err)
      if (err.status) {
        return response.status(err.status).send({
          error: {
            message: 'Não foi possível criar o equipamento'
          }
        })
      }

      return response.status(404).send({
        error: {
          message: 'Não foi possível criar o equipamento'
        }
      })
    }
  }

  async show({ params, response }) {
    try {
      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        params.serialnumber
      )

      await equipment.load('unity')
      await equipment.load('company')
      await equipment.load('file')

      return equipment
    } catch (err) {
      return response.status(404).send({
        error: {
          message: 'Não foi possível listar o equipamento'
        }
      })
    }
  }

  async update({ params, request, response }) {
    try {
      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        params.serialnumber
      )

      const data = request.only([
        'name',
        'model',
        'serialnumber',
        'unity_id',
        'company_id',
        'status',
        'active',
        'position',
        'ip',
        'sha1Curve',
        'hasFile',
        'grain',
        'idealRangeMin',
        'idealRangeMax'
      ])

      equipment.merge(data)

      await equipment.save()

      return equipment
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível atualizar o equipamento'
        }
      })
    }
  }

  async destroy({ params, response }) {
    try {
      const equipment = await Equipment.findOrFail(params.id)

      equipment.delete()
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível deletar o equipamento'
        }
      })
    }
  }
}

module.exports = EquipmentController

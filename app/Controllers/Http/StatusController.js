'use strict'

class StatusController {
  async index ({ request, response }) {
    return response.status(200).send({
      message: 'ok'
    })
  }
}

module.exports = StatusController

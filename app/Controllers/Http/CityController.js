'use strict'
const Axios = use('axios')

const City = use('App/Models/City')
const State = use('App/Models/State')

class StateController {
  async store ({ request, response }) {
    try {
      const states = await State.all()
      const aux = states.toJSON()

      for (const item of aux) {
        Axios.get(
          `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${item.id}/municipios`
        ).then(async res => {
          for (const city of res.data) {
            const newCity = await City.create({
              uf_id: item.id,
              nome: city.nome
            })
          }
        })
      }

      return states.toJSON()
    } catch (err) {
      console.log(err)
      return response.status(404).send({
        error: {
          message: 'Não foi possível criar as cidades'
        }
      })
    }
  }

  async index ({ params, response }) {
    try {
      const cities = await City.query()
        .where('uf_id', params.uf_id)
        .fetch()

      return cities
    } catch (err) {
      response.status(404).send({
        error: {
          message: 'Não foi possível resgatar as cidades'
        }
      })
    }
  }
}
module.exports = StateController

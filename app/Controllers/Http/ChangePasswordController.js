'use strict'

const Hash = use('Hash')

class ChangePasswordController {
  async update({ request, response, auth }) {
    const user = auth.current.user

    // hash and save new password
    user.password = request.input('newPassword')
    await user.save()

    return response.json({
      message: 'Senha atualizada.'
    })
  }
}

module.exports = ChangePasswordController

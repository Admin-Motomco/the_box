'use strict';

const Equipment = use('App/Models/Equipment')
const User = use('App/Models/User')

class EquipmentByCompanyController {
  async index ({ response, auth, params }) {
    try {
      const { id } = auth.user

      const requestUser = await User.findOrFail(id)

      if (
        (await requestUser.is('master')) ||
        (await requestUser.is('commercial'))
      ) {
        const { company_id } = params

        const equipments = await Equipment.query()
          .where('company_id', company_id)
          .with('company')
          .with('unity')
          .with('file')
          .fetch()

        return equipments
      } else {
        const equipments = await Equipment.query()
          .where('company_id', requestUser.company_id)
          .with('company')
          .with('unity')
          .with('file')
          .fetch()

        return equipments
      }
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível resgatar os equipamentos.'
        }
      })
    }
  }
}

module.exports = EquipmentByCompanyController

'use strict'

const Measurement = use('App/Models/Measurement')
const Equipment = use('App/Models/Equipment')

class MeasureByGrainController {
  async index({ response, params }) {
    try {
      const grainMeasurements = await Measurement.query()
        .where('serialnumber', params.serialnumber)
        .distinct('grain')
        .fetch()

      const equipmentStatus = await Equipment.findByOrFail('serialnumber', params.serialnumber)

      const result = []
      // eslint-disable-next-line prefer-const
      const measureResponse = []

      const promise = grainMeasurements.toJSON().map(async element => {
        const measure = await Measurement.query().where({ serialnumber: params.serialnumber, grain: element.grain }).limit(1)
          .orderBy('id', 'desc')
          .fetch()

        const formattedMeasure = measure.toJSON()

        measureResponse.push({
          grain: formattedMeasure[0].grain,
          humidity: formattedMeasure[0].humidity,
          ph: formattedMeasure[0].ph,
          temperature: formattedMeasure[0].temperature,
          status: equipmentStatus.status
        })
      })

      await Promise.all(promise)

      return measureResponse
    } catch (error) {
      console.log(error)
      return response.status(404).send({
        error: {
          message: 'Não encontrado'
        }
      })
    }
  }

  async show({ request, response, params }) {
    try {
      const { grain } = request.get()
      const measurements = await Measurement.query()
        .where({
          serialnumber: params.serialnumber,
          grain
        })
        .limit(50)
        .orderBy('id', 'DESC')
        .fetch()
      return measurements
    } catch (error) {
      console.log(error)
      return response.status(404).send({
        error: {
          message: 'Não encontrado'
        }
      })
    }
  }
}

module.exports = MeasureByGrainController

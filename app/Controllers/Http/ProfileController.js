'use strict'

const User = use('App/Models/User')

class ProfileController {
  async index ({ auth, response }) {
    try {
      const { id } = auth.user

      const user = await User.findOrFail(id)

      await user.load('company')
      await user.load('unity')
      await user.load('roles')

      return user
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível resgatar seu perfil.'
        }
      })
    }
  }
}

module.exports = ProfileController

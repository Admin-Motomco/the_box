'use strict';

const User = use('App/Models/User')

class SessionController {
  async store ({ request, response, auth }) {
    const { email, password } = request.all()

    const token = await auth.attempt(email, password)
    const user = await User.query()
      .with('company')
      .where('email', email)
      .fetch()

    return response.send({
      token,
      user
    })
  }
}

module.exports = SessionController

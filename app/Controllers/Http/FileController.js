'use strict'

const File = use('App/Models/File')
const Equipment = use('App/Models/Equipment')
const Helpers = use('Helpers')

class FileController {
  async show ({ params, response }) {
    const file = await File.findOrFail(params.id)

    return response.download(
      Helpers.tmpPath(`uploads/${file.file}.${file.subtype}`)
    )
  }

  async store ({ request, response }) {
    try {
      if (!request.file('file')) return
      const serialnumber = request.input('serialnumber')

      if (!serialnumber) return

      const upload = request.file('file', { size: '2mb' })

      const fileName = `${serialnumber}.${upload.subtype}`

      await upload.move(Helpers.tmpPath('uploads'), {
        name: fileName,
        overwrite: true
      })

      if (!upload.moved()) {
        throw upload.error()
      }

      const fileExists = await File.findBy('file', serialnumber)
      if (!fileExists) {
        const equipment = await Equipment.findByOrFail(
          'serialnumber',
          serialnumber
        )

        const file = await File.create({
          file: serialnumber,
          name: upload.clientName,
          type: upload.type,
          subtype: upload.subtype,
          equipment_id: equipment.id
        })
        // Atualizar o equipamento com o Id desse arquivo.

        return file
      }

      const equipment = await Equipment.findByOrFail(
        'serialnumber',
        serialnumber
      )

      fileExists.file = serialnumber
      fileExists.name = upload.clientName
      fileExists.type = upload.type
      fileExists.subtype = upload.subtype
      fileExists.equipment_id = equipment.id

      equipment.merge({ hasFile: true })

      await fileExists.save()
      await equipment.save()

      return fileExists
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Erro no upload de arquivo, este equipamento existe?'
        }
      })
    }
  }

  async update ({ params, response, request }) {
    try {
      const file = await File.findOrFail(params.id)

      const data = request.only('downloaded')

      file.merge(data)

      await file.save()

      return file
    } catch (err) {
      return response.status(err.status).send({
        error: {
          message: 'Não foi possível atualizar o arquivo'
        }
      })
    }
  }
}

module.exports = FileController

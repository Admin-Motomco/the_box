'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Equipment extends Model {
  file () {
    return this.hasOne('App/Models/File')
  }

  company () {
    return this.belongsTo('App/Models/Company')
  }

  unity () {
    return this.belongsTo('App/Models/Unity')
  }
}

module.exports = Equipment

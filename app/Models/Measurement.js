'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Measurement extends Model {
  static get hidden () {
    return ['password', 'data.signature', 'data.pubkey', 'data.idcalib']
  }
}

module.exports = Measurement

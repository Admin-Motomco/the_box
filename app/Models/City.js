'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class City extends Model {
  state () {
    this.hasOne('App/Models/State')
  }

  companies () {
    this.hasMany('App/Models/Company')
  }
}

module.exports = City

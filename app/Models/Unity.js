'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Unity extends Model {
  users () {
    return this.hasMany('App/Models/User')
  }

  equipments () {
    return this.hasMany('App/Models/Equipment')
  }

  company () {
    return this.belongsTo('App/Models/Company')
  }
}

module.exports = Unity

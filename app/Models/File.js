'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')

class File extends Model {
  static get computed () {
    return ['url']
  }

  equipment () {
    return this.belongsTo('App/Models/Equipment')
  }

  getUrl ({ id }) {
    return `${Env.get('APP_URL')}/the-box/files/${id}`
  }
}

module.exports = File

'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Company extends Model {
  unities () {
    return this.hasMany('App/Models/Unity')
  }

  users () {
    return this.hasMany('App/Models/User')
  }

  equipments () {
    return this.hasMany('App/Models/Equipment')
  }

  city () {
    return this.belongsTo('App/Models/City')
  }

  state () {
    return this.belongsTo('App/Models/State')
  }
}

module.exports = Company

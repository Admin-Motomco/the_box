'use strict'

const Route = use('Route')

Route.get('the-box/status', 'StatusController.index')

Route.post('the-box/users', 'UserController.store').middleware([
    'auth',
    'is:(master or commercial or company_master)'
])
Route.put('the-box/users/:id', 'UserController.update').middleware([
    'auth',
    'is:master'
])
Route.get('the-box/users', 'UserController.index').middleware([
    'auth',
    'is:(master or commercial)'
])
Route.get('the-box/users/:id', 'UserController.show').middleware([
    'auth'
])
Route.delete('the-box/users/:id', 'UserController.destroy').middleware([
    'auth',
    'is:(master or commercial)'
])

Route.post('the-box/sessions', 'SessionController.store')

Route.post('the-box/passwords', 'ForgotPasswordController.store')
Route.put('the-box/passwords', 'ForgotPasswordController.update')

Route.resource('the-box/permissions', 'PermissionController').middleware([
    'auth',
    'is:master'
])

Route.resource('the-box/roles', 'RoleController').middleware([
    'auth',
    'is:master'
])

Route.resource('the-box/companies', 'CompanyController').middleware([
    'auth',
    'is:(master or commercial)'
])

Route.resource('the-box/unities', 'UnityController')
    .middleware(['auth', 'is:(master or commercial)'])
    .except(['index'])
Route.get(
    'the-box/unities/companies/:company_id',
    'UnityController.index'
).middleware(['auth', 'is:(master or commercial)'])

Route.resource('the-box/equipments', 'EquipmentController')
    .middleware(['auth', 'is:(master or commercial)'])
    .except(['show', 'update'])
Route.get(
    'the-box/equipments/:serialnumber',
    'EquipmentController.show'
).middleware(['auth', 'is:(master or commercial)'])
Route.put(
    'the-box/equipments/:serialnumber',
    'EquipmentController.update'
).middleware(['auth', 'is:(master or commercial)'])

Route.resource(
    'the-box/equipments/company/:company_id',
    'EquipmentByCompanyController'
).middleware(['auth', 'is:(master or commercial or company_master)'])

Route.resource(
    'the-box/equipments/unity/:unity_id',
    'EquipmentByUnityController'
).middleware(['auth', 'is:(master or commercial or company_master)'])

Route.get(
    'the-box/measurements/:serialnumber',
    'MeasurementController.index'
).middleware(['auth', 'is:(master or commercial or company_master)'])
Route.post('the-box/measurements', 'MeasurementController.store')

Route.get(
    'the-box/measurements/individual/:serialnumber',
    'MeasurementController.show'
).middleware(['auth', 'is:(master or commercial or company_master)'])
Route.put(
    'the-box/measurements/individual/:id',
    'MeasurementController.update'
).middleware(['auth', 'is:(master or commercial)'])
Route.delete(
    'the-box/measurements/individual/:id',
    'MeasurementController.destroy'
).middleware(['auth', 'is:master'])

Route.get('the-box/files/:id', 'FileController.show').middleware(['auth'])
Route.post('the-box/files', 'FileController.store').middleware([
    'auth',
    'is:(master or commercial)'
])
Route.put('the-box/files/:id', 'FileController.update').middleware(['auth'])

Route.resource('the-box/profile', 'ProfileController').middleware('auth')

// Alimenta a tabela de estados, com os dados da API do IBGE.
Route.resource('the-box/states', 'StateController').middleware([
    'auth',
    'is:(master or commercial)'
])

// Alimenta as cidades de acordo com os estados vindos da API do IBGE.
Route.post('the-box/cities', 'CityController.store').middleware([
    'auth',
    'is:(master or commercial)'
])
Route.get('the-box/cities/states/:uf_id', 'CityController.index').middleware([
    'auth',
    'is:(master or commercial)'
])

Route.get(
    'the-box/geolocations/equipments/:serialnumber',
    'GeolocationController.index'
).middleware(['auth', 'is:master'])
Route.get('the-box/geolocations/:serialnumber', 'GeolocationController.show').middleware(['auth'])
Route.post('the-box/geolocations', 'GeolocationController.store')

Route.put('the-box/change_password', 'ChangePasswordController.update').middleware(['auth'])

Route.get('the-box/grain/measurements/:serialnumber', 'MeasureByGrainController.index').middleware(['auth'])
Route.get('the-box/last-measurements/equipments/:serialnumber/grain', 'MeasureByGrainController.show').middleware(['auth'])
'use strict';

const Equipment = use('App/Models/Equipment')

/*
|--------------------------------------------------------------------------
| Websocket
|--------------------------------------------------------------------------
|
| This file is used to register websocket channels and start the Ws server.
| Learn more about same in the official documentation.
| https://adonisjs.com/docs/websocket
|
| For middleware, do check `wsKernel.js` file.
|
*/

const Server = use('Server')
const io = use('socket.io')(Server.getInstance())

io.on('connection', async function (socket) {
  const serialnumber = socket.handshake.headers.serialnumber

  attStatus(serialnumber, true)
  //  console.log(`Conectando... ${serialnumber}`)

  socket.on('disconnect', async function () {
    attStatus(serialnumber, false)
    //  console.log(`Desconectando... ${serialnumber}`)
  })
})

async function attStatus (serialnumber, status) {
  try {
    const equipment = await Equipment.findByOrFail(
      'serialnumber',
      serialnumber
    )

    equipment.status = status
    await equipment.save()
  } catch (error) {
    console.log(error)
  }
}

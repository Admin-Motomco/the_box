#!/bin/bash
DIRECTORY=/home/ec2-user/the_box

if [ -d "$DIRECTORY" ]; then
  echo "Exists"
else
  sudo mkdir /home/ec2-user/the_box
fi

cd /home/ec2-user/the_box
sudo npm install
adonis migration:run --force
